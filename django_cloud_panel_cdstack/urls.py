from django.urls import path

from . import views

urlpatterns = [
    path("hosts", views.list_hosts),
    path("hosts/create", views.modal_host_create),
    path("host/<str:host>", views.edit_host),
    path("host/<str:host>/delete", views.modal_host_delete),
    path("instances", views.list_instances),
    path("instances/create", views.modal_instance_create),
    path("instance/<str:instance>", views.edit_instance),
    path("instance/<str:instance>/delete", views.modal_instance_delete),
    path("groups", views.list_groups),
    path("groups/create", views.modal_group_create),
    path("group/<str:group>", views.edit_group),
    path("group/<str:group>/delete", views.modal_group_delete),
    path("meta-datas", views.list_meta_datas),
    path("meta-datas/create", views.modal_meta_data_create),
    path("meta-data/<str:meta_data>", views.edit_meta_data),
    path("meta-data/<str:meta_data>/delete", views.modal_meta_data_delete),
]
