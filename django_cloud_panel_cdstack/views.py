from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)


@admin_group_required("CMDB")
def list_hosts(request):
    template = loader.get_template("django_cloud_panel_cdstack/list_hosts.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("List Hosts")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def edit_host(request, host):
    template = loader.get_template("django_cloud_panel_cdstack/edit_host.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Edit Host") + " " + host

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def modal_host_create(request):
    template = loader.get_template("django_cloud_panel_cdstack/modal_host_create.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Create Host")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def modal_host_delete(request, host):
    template = loader.get_template("django_cloud_panel_cdstack/modal_host_delete.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Delete Host") + " " + host

    template_opts["host"] = host

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def list_instances(request):
    template = loader.get_template("django_cloud_panel_cdstack/list_instances.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("List Instances")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def edit_instance(request, instance):
    template = loader.get_template("django_cloud_panel_cdstack/edit_instance.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Edit Instance") + " " + instance

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def modal_instance_create(request):
    template = loader.get_template(
        "django_cloud_panel_cdstack/modal_instance_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Create Instance")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def modal_instance_delete(request, instance):
    template = loader.get_template(
        "django_cloud_panel_cdstack/modal_instance_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Delete Instance") + " " + instance

    template_opts["instance"] = instance

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def list_groups(request):
    template = loader.get_template("django_cloud_panel_cdstack/list_groups.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("List Groups")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def edit_group(request, group):
    template = loader.get_template("django_cloud_panel_cdstack/edit_group.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Edit Group") + " " + group

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def modal_group_create(request):
    template = loader.get_template("django_cloud_panel_cdstack/modal_group_create.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Create Group")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def modal_group_delete(request, group):
    template = loader.get_template("django_cloud_panel_cdstack/modal_group_delete.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Delete Group") + " " + group

    template_opts["group"] = group

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def list_meta_datas(request):
    template = loader.get_template("django_cloud_panel_cdstack/list_meta_datas.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("List Meta-Datas")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def edit_meta_data(request, meta_data):
    template = loader.get_template("django_cloud_panel_cdstack/edit_meta_data.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Edit Meta-Data") + " " + meta_data

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def modal_meta_data_create(request):
    template = loader.get_template(
        "django_cloud_panel_cdstack/modal_meta_data_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Create Meta-Data")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("CMDB")
def modal_meta_data_delete(request, meta_data):
    template = loader.get_template(
        "django_cloud_panel_cdstack/modal_meta_data_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Config-Management DB")
    template_opts["content_title_sub"] = _("Delete Meta-Data") + " " + meta_data

    template_opts["meta_data"] = meta_data

    return HttpResponse(template.render(template_opts, request))
